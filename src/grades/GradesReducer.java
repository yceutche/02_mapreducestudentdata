package grades;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class GradesReducer extends
		Reducer<IntWritable, IntWritable, IntWritable, FloatWritable> {
	@Override
	public void reduce(IntWritable key, Iterable<IntWritable> values,
			Context context) throws IOException, InterruptedException {

		float sum = 0;
		int count = 0;
		for (IntWritable value : values) {

			sum += value.get();
			count += 1;
		}
		float result = sum / count;

		context.write(key, new FloatWritable(result));
	}

}
