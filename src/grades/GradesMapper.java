package grades;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class GradesMapper extends Mapper<Text, Text, IntWritable, IntWritable> {

	private IntWritable year_int = null;
	private IntWritable grade_int = null;
	
	public void map( Text key, Text value, Context context) throws IOException, InterruptedException {
		//Auslesen des Jahres und der Note aus einem String wie "285397 23.08.2013 19"	  
		System.out.println( "VALUE: " + value);
		String year_str = value.toString().substring(10,13);
		String grade_str = value.toString().substring(14,15);
		
		year_int = new IntWritable(Integer.parseInt(year_str));
		grade_int = new IntWritable(Integer.parseInt(grade_str));
		
		context.write(year_int, grade_int);
	}

}
