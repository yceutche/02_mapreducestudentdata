package grades;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class GradesDriver extends Configured implements Tool {

	private final static Logger LOG = Logger.getLogger(GradesDriver.class
			.getName());

	public static void main(String args[]) {
		int res = 1;// wenn 1 nicht verändert wird, endet der Job nicht korrekt
		try {
			res = ToolRunner.run(new Configuration(), new GradesDriver(), args);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Fehler beim Ausführen des Jobs!");
			e.printStackTrace();
		}
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {
		LOG.log(Level.INFO, "Starte Map-Reduce-Job 'GradesDriver' ...");

		Configuration conf = this.getConf();

		Job job = null;

		try {
			job = job.getInstance(conf);

		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Fehler bei Instanzierung des jobs!");
			e.printStackTrace();
		}

		// Haddop soll ein verfügbares Jar verwenden, das die Klasse
		// GradesDriver enthählt.
		job.setJarByClass(GradesDriver.class);

		// Mapper und Reducer Klasse werden festgelegt
		job.setMapperClass(GradesMapper.class);
		job.setReducerClass(GradesReducer.class);

		// Ausgabetypen werden festgelegt
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(FloatWritable.class);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setInputFormatClass(KeyValueTextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		// Der Pfad, aus dem Hadoop die Eingabedateien liest, wird als erstes
		// Argument beim Starten des JAR übergeben
		try {
			FileInputFormat.addInputPath(job, new Path(args[1]));
		} catch (IllegalArgumentException e) {
			LOG.log(Level.SEVERE, "Fehler (Argument) beim setzen des Eingabepfades");
			e.printStackTrace();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Fehler (IO) beim setzen des Eingabepfades");
			e.printStackTrace();
		}
		
		//der Ausgabeordner wird als zweites Argument übergeben 
		FileOutputFormat.setOutputPath(job, new Path(args[2]));
		boolean result = false;
		
		try{
			// Führe den Job aus und warte, bis er beendet wurde
			result = job.waitForCompletion(true);
			
		}catch(ClassNotFoundException e){
			LOG.log(Level.SEVERE, "Fehler (IO) beim setzen des Eingabepfades");
			e.printStackTrace();
		}catch(IOException e){
			LOG.log(Level.SEVERE, "Fehler (IO) beim setzen des Eingabepfades");
			e.printStackTrace();
		}catch(InterruptedException e){
			LOG.log(Level.SEVERE, "Fehler (IO) beim setzen des Eingabepfades");
			e.printStackTrace();
		}
		LOG.log(Level.INFO, "Fertig");
		return 0;
	}
}
